# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version - ruby-3.0.2

* System dependencies - psql

* Configuration - 
    gem install bundler
    bundle install

* Database creation
  rake db:create

* Database initialization
  rake db:schema:load
  rake db:migrate

* How to run the test suite
  rake test

* Start rails server
  rails s

* Deployment instructions

* ...
